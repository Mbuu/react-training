import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Likes from "./components/Likes/Likes";
import Menu from "./components/Menu/Menu";
import SavedMp3 from "./components/SavedTracks/SavedMp3";
import NotFound from "./components/NotFound/NotFound";
import Profile from "./components/Profile/Profile";

class App extends Component {

    render() {
        return (
            <Fragment>
                <h1>Header</h1>
                <Switch>
                    <Route path='/likes' component={Likes}/>
                    <Route path='/saved/:mp3Id' component={SavedMp3}/>
                    <Route path='/saved' component={SavedMp3}/>
                    <Route path='/profile' component={Profile}/>
                    <Route exact path='/' component={Menu}/>
                    <Route path='*' component={NotFound}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
