import React, {Component, Fragment} from 'react';

class Body extends Component {
    render() {
        const {addLike, addDislike} = this.props;
        return (
            <Fragment>
                <h1>Body</h1>
                <input type='button' onClick={() => addLike()} value='Like'/>
                <label>click to add like</label>
                <br/>
                <br/>
                <input type='button' onClick={() => addDislike()} value='Dislike'/>
                <label>click to delete like</label>
            </Fragment>
        );
    }
}

export default Body;
