import React, {Component} from 'react';
import Header from "../Header/Header";
import Body from "../Body/Body";
import Footer from "../Footer/Footer";

class Likes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            likes: 0,
            dislikes: 0,
            name: 'Boar'
        };

        /*this.newLike = this.newLike.bind(this);
        this.newDislike = this.newDislike.bind(this);*/
    }

    render() {
        const {likes, dislikes, name} = this.state;

        return (
            <div>
                <Header name={name} likes={likes} dislikes={dislikes}/>
                <Body addLike={this.newLike} addDislike={this.newDislike}/>
                <Footer copyright='MIT' date={new Date()} />
            </div>
        );
    }

    newLike = () => {
        this.setState({
            likes: this.state.likes + 1
        })
    };

    newDislike = () => {
        this.setState({
            dislikes: this.state.dislikes + 1
        })
    };
}

export default Likes;
