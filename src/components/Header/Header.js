import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

class Header extends Component {
    render() {
        const {name, likes, dislikes, reduxlikes} = this.props;
        return (
            <Fragment>
                <h1>Hello, {name}</h1>
                <h2>Likes: {likes}</h2>
                <h2>Likes: {reduxlikes}</h2>
                <h2>Dislikes: {dislikes}</h2>
            </Fragment>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        reduxlikes: store.likes
    }
};

export default connect(mapStateToProps)(Header);
