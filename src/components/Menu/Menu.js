import React, {Component, Fragment} from 'react';
import {Link} from "react-router-dom";

class Menu extends Component {

    render() {
        return (
            <Fragment>
                <h1>Menu</h1>
                <ul>
                <li><Link to='/likes' title='http://localhost:3000/likes'>Likes</Link></li>
                <li><Link to='/saved' title='http://localhost:3000/saved'>Saved MP3</Link></li>
                <li><Link to='/profile' title='http://localhost:3000/profile'>Profile</Link></li>
            </ul>
            </Fragment>
        );
    }
}

export default Menu;