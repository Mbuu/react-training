import {CHANGE_USER_AGE, CHANGE_USER_NAME} from '../actions/types';

const initialState = {
    name: 'Valera',
    age: 54,

};

function userReducer (state = initialState, action) {
    switch (action.type) {
        case CHANGE_USER_NAME:
            let newName = action.payload + 'hi';
            return {...state, name: newName};
        case CHANGE_USER_AGE:
            return {...state, age: action.payload};
        default:
            return state;
    }
}

export default userReducer;